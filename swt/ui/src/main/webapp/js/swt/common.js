var common = (function() {
	var obj = {};
	
	obj.tokenKey = "Authorization";
	obj.restservices = restservicesPath;
	

	/*
	 * http://tosbourn.com/a-fix-for-window-location-origin-in-internet-explorer/
	 */
    if (!window.location.origin) {
		window.location.origin = window.location.protocol + "//"
				+ window.location.hostname
				+ (window.location.port ? ':' + window.location.port : '');
	}

	obj.ajax = function(request, options) {
		options = defaultOptions(options);
		
		updateUrl(request);
		updateContentType(request);
		
		if(options.auth) {
			addAuthorization(request);
			loginForInvalidToken(request);
		}
		
		/*
		 * This is a fix to prevent caching of custom ajax calls in IE.
		 * 
		 */
		request.cache = false;
		
		$.ajax(request);
	};
		
	obj.blockUI = function() {
		var imageUrl = obj.toFullUrl('/images/ajax-loader.gif');
		var image = '<img src="' + imageUrl + '" />';
		
		$.blockUI({
			message : image,
			centerX : false,
			centerY : false,
			css : {
				width : '4%',
				border : '0px solid #FFFFFF',
				cursor : 'wait',
				backgroundColor : 'transparent',
				top : '48%',
				left : '48%'
			},
			overlayCSS : {
				backgroundColor : '#000',
				opacity : 0.2,
				cursor : 'wait'
			},
			baseZ : 2000
		});
	};

	obj.getUrlParameter = function(name) {
	    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	    if (results==null){
	       return null;
	    }
	    else{
	       return results[1] || 0;
	    }
	};
	
	obj.logActivity = function(data) {
		
		var referral = $.cookie('referralConsumer');
		if(referral & !data.referralConsumer) {
			data.referralConsumer = referral;
		}
		
		obj.ajax({
			type : 'PUT',
			url : '/activityLog',
			data : JSON.stringify(data)
		});
	};
	
	obj.shareOnFb = function(data, successCallback, errorCallback) {
		
		obj.ajax({
			type : 'PUT',
			url : '/consumer/shareByFb',
			data : JSON.stringify(data),
			success : function(response) {
				if(successCallback != null) {
					successCallback(response);
				}
			},
			error : function(xhr) {
				if(errorCallback != null) {
					errorCallback(xhr);
				}
			}
		});
	};
	
	obj.shareOnTwitter = function(data, successCallback, errorCallback) {
		
		obj.ajax({
			type : 'PUT',
			url : '/consumer/shareByTwitter',
			data : JSON.stringify(data),
			success : function(response) {
				if(successCallback != null) {
					successCallback(response);
				}
			},
			error : function(xhr) {
				if(errorCallback != null) {
					errorCallback(xhr);
				}
			}
		});
	};
	
	obj.init = function(node) {
		var $node;
		
		if(node) {
			$node = $(node);
		} else {
			$node = $(document);
		}
		
		initImage($node);
		initCarousel($node);
	};
	
	obj.loadHtml = function(request) {
		if(request && request.url) {
			request.url = obj.toFullUrl(request.url);
			$.get(request.url, function(content) {
				$(request.node).empty();
				$(request.node).html(content);
				obj.init(request.node);
				if(typeof ga !== 'undefined' && ga) {
                    ga('set', 'page', request.url);
                    ga('send', 'pageview');
				}
				if(request.success) {
					request.success();
				}
			});
		}
	};
	
	//returns Id of video
	//videoSource, videoURL parameter needed
	obj.getIdStringFromVideoURL= function(videoSource,videoURL){
		var arr=[];
		var videoIdString={};
		if(videoSource=="Vimeo" ||videoSource=="vimeo"){
			arr = videoURL.split("/");
			videoIdString = arr[arr.length-1];
		}
		else if(videoSource=="Youtube" ||videoSource=="youtube"){
			if(videoURL.search("watch")!= -1)
				arr = videoURL.split("=");
			else if(videoURL.search("embed")!= -1)
				arr = videoURL.split("/");
			videoIdString = arr[arr.length-1];
		}
		return videoIdString;
	};
	
	obj.toFullUrl = function(url) {
		if(contextPath && contextPath !== '/' )
			return contextPath + url;
		
		return url;
			
	};

	obj.toAbsoluteUrl = function(url) {
		return window.location.origin + obj.toFullUrl(url);
	};
	
	obj.collapsible = function(obj) {
		 $(obj + ' h4').each(function() {
	            var tis = $(this), state = false, answer = tis.next('div').slideUp();
	            tis.click(function() {
	                state = !state;
	                answer.slideToggle(state);
	                tis.toggleClass('active', state);
	            });
	        });
	};
	
	obj.removeURLParameter = function(parameter, accessUrl) {
		
		var url;
		if(accessUrl != null) {
			url = accessUrl;
		} else {
			url = document.URL;
		}
		//prefer to use l.search if you have a location/link object
	    var urlparts= url.split('?');   
	    if (urlparts.length>=2) {

	        var prefix= encodeURIComponent(parameter)+'=';
	        var pars= urlparts[1].split(/[&;]/g);
	        //reverse iteration as may be destructive
	        for (var i= pars.length; i-- > 0;) {    
	            //idiom for string.startsWith
	            if (pars[i].lastIndexOf(prefix, 0) !== -1) {  
	                pars.splice(i, 1);
	            }
	        }

	        url= urlparts[0]+'?'+pars.join('&');
	        return url;
	    } else {
	        return url;
	    }
	};
	
	obj.queryString = function(){
		var url = document.URL;
		var arr = url.split("?");
		if(arr.length>1){
			return "?"+arr[1];
		}
		return "";
	};
	
	//to upload the file on server
	obj.uploadFile = function(param) {
		
		var formData = new FormData();
		formData.append("file", param.file, param.filename);
		formData.append("folder", param.folder);
		
		obj.ajax({
			type : 'POST',
			url : '/file',
			data : formData,
			success : function(response) {
				if(param.success != null) {
					param.success(response);
				}
			},
			error : function(xhr) {
				if(param.error != null) {
					param.error(xhr);
				}
			},
			processData : false,
			contentType : false
		});
	};
	
	obj.generateUUID = function() {
	    var d = new Date().getTime();
	    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
	        var r = (d + Math.random() * 16) % 16 | 0;
	        d = Math.floor(d / 16);
	        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
	    });
	    return uuid;
	};
	obj.toURL = function(fileurl) {
		
		/*
		 * Some of the dynamic urls are already appended with /file (Though it should not be.)
		 */
		if(!startsWith(fileurl, '/file'))
			fileurl = '/file' + fileurl;
		
		return obj.restservices + fileurl;
	};
	
	function updateUrl(request) {
		request.url = obj.restservices + request.url;
	}

	function updateContentType(request) {
		if(!request.contentType && request.contentType !== false)
			request.contentType = 'application/json';
	}

	function loginForInvalidToken(request) {
		var error = request.error;
		request.error = function(xhr) {
			if(xhr.status == 401) {
				var parameter = obj.queryString();
				location.href = obj.toFullUrl('/html/home.html'+parameter);
			} else if (error) {
				error(xhr);
			}
		};
	}

	function defaultOptions(options) {
		options = options || {};
		
		// Authorize if nothing is said
		if(!options.auth && options.auth !== false)
			options.auth = true;

		return options;
	}

	function addAuthorization(request) {
		var bearerToken = $.cookie(obj.tokenKey);
		
		if(bearerToken && bearerToken !== '') {
			if(!request.headers) {
				request.headers = {};
			}
			
			request.headers[obj.tokenKey] = bearerToken;
		}
	}
	
	function initCarousel($node) {
		if($().carousel) {
			$node.find('.carousel').carousel({
				interval: false
			});
		}
	}
	
	function initImage($node) {
		$node.find('img[fileurl]').each(function(index, img){
			img.src = obj.toURL($(img).attr('fileurl'));
		});
		$node.find('div[background-url]').each(function(index, div){
			var url = obj.toURL($(div).attr('background-url'));
			$(div).css('background-image', 'url(' + url +')');
		});
	}
    
	$('#home_consumer').click(function(e){
	    e.preventDefault();
		brands.load();
	});
	
	
	$('#closeBtnOk').click(function() {
		location = self.location.href;
	});
	

	
	function startsWith(string, str) {
		return string.slice(0, str.length) == str;
	}
	
	$(document).ajaxStart(obj.blockUI).ajaxStop($.unblockUI);
	$(document).ready(function() {
		obj.init();
	});
	
	return obj;	
})();
